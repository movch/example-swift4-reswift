//
//  PostViewController.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 07.06.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import UIKit
import ReSwift

final class PostViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    private let activityView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupActivityIndicator()
        store.dispatch(SetLoading())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        store.subscribe(self) {
            $0.select {
                $0.postState
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        store.unsubscribe(self)
    }
    
    // MARK: View state helpers
    
    private func setupActivityIndicator() {
        activityView.center = self.view.center
        self.view.addSubview(activityView)
    }
    
    fileprivate func setLoadingState() {
        titleLabel.isHidden = true
        contentLabel.isHidden = true
        self.title = "Loading..."
        
        activityView.startAnimating()
    }
    
    fileprivate func setFinishLoadingState() {
        activityView.stopAnimating()
        activityView.isHidden = true
        
        titleLabel.isHidden = false
        contentLabel.isHidden = false
    }
}

// MARK: - StoreSubscriber

extension PostViewController: StoreSubscriber {
    func newState(state: PostState) {
        switch state.post {
        case .loading:
            setLoadingState()
        case .finished(let post):
            self.title = post.title
            
            titleLabel.text = post.title
            contentLabel.text = post.body
            
            setFinishLoadingState()
        default:
            break
        }
    }
}
