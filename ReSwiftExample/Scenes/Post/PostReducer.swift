//
//  PostsReducer.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 08.06.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import ReSwift

struct PostReducer {
    var networkService: NetworkService
    
    func handle(action: Action, state: PostState?) -> PostState {
        switch action {
        case let action as FetchPost:
            fetchPost(withId: action.id)
            break
        case let action as SetPost:
            return PostState(post: Result.finished(action.post))
        case _ as SetLoading:
            return PostState(post: .loading)
        default:
            break
        }
        
        return state ?? PostState(post: .loading)
    }
    
    private func fetchPost(withId id: Int) {
        networkService.fetchPost(withID: id) { post, error in
            if error == nil, let post = post {
                DispatchQueue.main.async {
                    store.dispatch(SetPost(post: post))
                }
            }
        }
    }
}
