//
//  PostActions.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 08.06.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import ReSwift

struct FetchPost: Action {
    let id: Int
}

struct SetLoading: Action {}

struct SetPost: Action {
    let post: Post
}
