//
//  PostState.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 08.06.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import ReSwift

struct PostState {
    var post: Result<Post> = .loading
}
