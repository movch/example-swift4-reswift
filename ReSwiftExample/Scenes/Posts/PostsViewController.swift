//
//  PostsViewController.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 31.05.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import ReSwift
import RxCocoa
import RxDataSources
import RxSwift
import UIKit

final class PostsViewController: UIViewController, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    private let activityView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    private let disposeBag = DisposeBag()
    private let dataSource = BehaviorRelay<[Post]>(value: [])
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Posts"
        
        store.dispatch(FetchPosts())
        
        setupActivityIndicator()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        store.subscribe(self) {
            $0.select {
                $0.postsState
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        store.unsubscribe(self)
    }
    
    // MARK: View state helpers
    
    private func setupTableView() {
        dataSource
            .bind(to: tableView.rx.items(cellIdentifier: "PostCell")) { index, model, cell in
                cell.textLabel?.text = model.title
            }
            .disposed(by: disposeBag)
        
        tableView.rx
            .modelSelected(Post.self)
            .subscribe(onNext:  { post in
                store.dispatch(FetchPost(id: post.id))
                store.dispatch(RouterAction(destination: .post))
            })
            .disposed(by: disposeBag)
    }
    
    private func setupActivityIndicator() {
        activityView.center = self.view.center
        self.view.addSubview(activityView)
    }
    
    fileprivate func setLoadingState() {
        tableView.isHidden = true
        activityView.startAnimating()
    }
    
    fileprivate func setFinishLoadingState() {
        activityView.stopAnimating()
        activityView.isHidden = true
        tableView.isHidden = false
    }
}

// MARK: - StoreSubscriber

extension PostsViewController: StoreSubscriber {
    func newState(state: PostsState) {
        switch state.posts {
        case .loading:
            setLoadingState()
        case .finished(let posts):
            dataSource.accept(posts)
            setFinishLoadingState()
        default:
            break
        }
    }
}
