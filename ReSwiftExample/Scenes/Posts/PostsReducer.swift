//
//  PostsReducers.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 30.05.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import ReSwift

struct PostsReducer {
    var networkService: NetworkService
    
    func handle(action: Action, state: PostsState?) -> PostsState {
        switch action {
        case _ as FetchPosts:
            fetchPosts()
            break
        case let action as SetPosts:
            return PostsState(posts: Result.finished(action.posts))
        default:
            break
        }
        
        return state ?? PostsState(posts: .loading)
    }
    
    private func fetchPosts() {
        networkService.fetchPosts { posts, error in
            if error == nil {
                DispatchQueue.main.async {
                    store.dispatch(SetPosts(posts: posts))
                }
            }
        }
    }
}

