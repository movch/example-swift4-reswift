//
//  AppReducer.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 30.05.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import ReSwift

func appReducer(action: Action, state: AppState?) -> AppState {
    let networkService = NetworkService()
    let postsReducer = PostsReducer(networkService: networkService)
    let postReducer = PostReducer(networkService: networkService)
    
    return AppState(
        routingState: routerReducer(action: action, state: state?.routingState),
        postsState: postsReducer.handle(action: action, state: state?.postsState),
        postState: postReducer.handle(action: action, state: state?.postState)
    )
}
