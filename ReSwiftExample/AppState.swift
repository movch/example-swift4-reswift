//
//  AppState.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 30.05.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import ReSwift

struct AppState: StateType {
    var routingState: RouterState
    var postsState: PostsState
    var postState: PostState
}
