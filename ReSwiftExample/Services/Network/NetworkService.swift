//
//  NetworkService.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 30.05.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import Foundation

class NetworkService {
    let baseURL = "https://jsonplaceholder.typicode.com"
    
    func fetchPosts(completion: @escaping ([Post], Error?) -> ()) {
        guard let url = URL(string: "\(baseURL)/posts") else {
            return completion([], nil)
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let data = data,
                let obj = try? JSONDecoder().decode([Post].self, from: data)
            else {
                return completion([], error)
            }
            
            completion(obj, nil)
        }
        
        task.resume()
    }
    
    func fetchPost(withID id: Int, completion: @escaping (Post?, Error?) -> ()) {
        guard let url = URL(string: "\(baseURL)/posts/\(id)") else {
            return completion(nil, nil)
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let data = data,
                let obj = try? JSONDecoder().decode(Post.self, from: data)
                else {
                    return completion(nil, error)
            }
            
            completion(obj, nil)
        }
        
        task.resume()
    }
}
