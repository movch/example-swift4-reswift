//
//  RoutingReducer.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 07.06.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import ReSwift

func routerReducer(action: Action, state: RouterState?) -> RouterState {
    var state = state ?? RouterState()
    
    switch action {
    case let routingAction as RouterAction:
        state.navigationState = routingAction.destination
    default: break
    }
    
    return state
}
