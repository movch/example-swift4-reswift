//
//  AppRouter.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 07.06.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import ReSwift

enum RouterDestination: String {
    case posts = "PostsViewController"
    case post = "PostViewController"
}
