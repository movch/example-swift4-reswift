//
//  RouterState.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 07.06.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import ReSwift

struct RouterState: StateType {
    var navigationState: RouterDestination
    
    init(navigationState: RouterDestination = .posts) {
        self.navigationState = navigationState
    }
}
