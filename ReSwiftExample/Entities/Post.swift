//
//  Post.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 30.05.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import Foundation

struct Post: Decodable {
    var id: Int
    var title: String
    var body: String?
}
