//
//  Result.swift
//  ReSwiftExample
//
//  Created by Michail Ovchinnikov on 30.05.2018.
//  Copyright © 2018 Michail Ovchinnikov. All rights reserved.
//

import Foundation

enum Result<T> {
    case loading
    case failed
    case finished(T)
}
